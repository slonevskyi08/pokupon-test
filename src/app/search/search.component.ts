import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  btnValue: string = `Искать`;
  searchPhraseErrorValue: string = `Текстовое поле не может быть пустым`;
  searchSystemErrorValue: string = `Необходимо выбрать поисковую систему`;
  searchPhraseIsValid: boolean = true;
  searchSystemIsValid: boolean = true;
  disabledOption: string = `Укажите поисковую систему`;
  inputPlaceholder: string = `Ведите текст для поиска`;

  searchSystems = [
    {value: `https://www.google.com/search?q=`, viewValue: `google.com`},
    {value: `https://www.bing.com/search?q=`, viewValue: `bing.com`},
    {value: `https://www.ask.com/web?q=`, viewValue: `ask.com`}
  ];

  constructor() {
  }

  ngOnInit() {
  }

  doSearch(searchSystem: string, searchPhrase: string) {
    if (searchPhrase.length === 0) {
      this.searchPhraseIsValid = false;
    } else if (searchSystem === `null`) {
      this.searchSystemIsValid = false;
      this.searchPhraseIsValid = true;
    } else {
      window.open(`${searchSystem}${searchPhrase}`, `_blank`);
      this.searchPhraseIsValid = true;
      this.searchSystemIsValid = true;
    }
  }



}
